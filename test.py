import unittest
from app import app

class FlaskAppTestCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def test_main_route(self):
        response = self.app.get('/')
        self.assertIn(b'Welcome!', response.data)

    def test_hello_route(self):
        response = self.app.get('/how%20are%20you')
        self.assertIn(b'I am good, how about you?', response.data)

if __name__ == '__main__':
    unittest.main()
